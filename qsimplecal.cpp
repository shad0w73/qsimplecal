#include <csignal>
#include <QApplication>
#include <QCalendarWidget>
#include <QLockFile>
#include <QScreen>
#include <QStandardPaths>
#include <QTextCharFormat>

class calendar : public QApplication, public QCalendarWidget
{
public:
	calendar(int argc, char *argv[])
	: QApplication(argc, argv)
	{
		QTextCharFormat format;
		this->setWeekdayTextFormat(Qt::DayOfWeek::Saturday, format);
		this->setWeekdayTextFormat(Qt::DayOfWeek::Sunday, format);
		format.setFontWeight(QFont::Bold);
		this->setDateTextFormat(QDate::currentDate(), format);

		auto s = this->screenAt(QCursor::pos())->geometry();
		this->setGeometry(s.x() + s.width() - 225 - 2, 0, 225, 172);
		this->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
		this->setWindowFlags(Qt::WindowStaysOnTopHint);
		this->show();
	}
} *w;

void signal_handler(int signal)
{
	Q_UNUSED(signal);
	w->close();
}

int main(int argc, char *argv[])
{
	auto lockfile_path = QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation);
	if (lockfile_path.isEmpty())
		return EXIT_FAILURE;

	lockfile_path += "/qsimplecal.lock";
	auto lockfile = QLockFile(lockfile_path);
	lockfile.setStaleLockTime(86400);
	if (!lockfile.tryLock(0)) {
		if (lockfile.error() == QLockFile::LockFailedError) {
			qint64 pid; QString hostname; QString appname;
			if (lockfile.getLockInfo(&pid, &hostname, &appname))
				if (kill(pid, SIGTERM) == 0)
					return EXIT_SUCCESS;
		}
		return EXIT_FAILURE;
	}

	w = new calendar(argc, argv);
	std::signal(SIGINT,  signal_handler);
	std::signal(SIGTERM, signal_handler);
	int ret = w->exec();
	delete w;

	return ret;
}
