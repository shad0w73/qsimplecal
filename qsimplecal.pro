QT += widgets

TARGET = qsimplecal

target.path = /usr/local/bin
INSTALLS += target

SOURCES += \
    qsimplecal.cpp

DISTFILES += \
    LICENSE \
    README.md
